<?php

/**
 * @file
 * uw_vocab_feds_departments.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_vocab_feds_departments_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'define view for terms in feds_departments'.
  $permissions['define view for terms in feds_departments'] = array(
    'name' => 'define view for terms in feds_departments',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary feds_departments'.
  $permissions['define view for vocabulary feds_departments'] = array(
    'name' => 'define view for vocabulary feds_departments',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'edit terms in feds_departments'.
  $permissions['edit terms in feds_departments'] = array(
    'name' => 'edit terms in feds_departments',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
