<?php

/**
 * @file
 * uw_vocab_feds_departments.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_vocab_feds_departments_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Commercial Operations',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => '09a63cac-c501-4562-ba10-28a755724764',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/commercial-operations',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Event',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 6,
    'uuid' => '2d273ce6-d9b1-440d-abc8-ba3a5af3ca9e',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/event',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Marketing & Communications',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => '815c461e-58eb-4a8a-9125-fc05bff658d4',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/marketing-communications',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Campus Life',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 4,
    'uuid' => '839f12d3-dc63-496c-aebc-16db5cfecd86',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/campus-life',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Orientation',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '84581216-c79a-487c-a47f-e9c18760cb75',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/orientation',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Main Office',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 8,
    'uuid' => '8a1a017d-2f99-4e92-8b97-ee963e6b2494',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/main-office',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Advocacy',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 5,
    'uuid' => '99b0d47e-34d4-4ac6-a131-159d3dd9ddf8',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/advocacy',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Information Technology (IT)',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 9,
    'uuid' => 'a2c0104d-40c7-4078-824e-096c9320a76b',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/information-technology-it',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Accounting',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 10,
    'uuid' => 'a8f2e263-d651-4eae-91bf-95843dc947c3',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/accounting',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Executives',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 7,
    'uuid' => 'bf7936d4-6728-4cde-812b-af18c6ffbbb2',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/executives',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Services',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'f36052b0-22ce-4b16-84d9-fdd40f23c03e',
    'vocabulary_machine_name' => 'feds_departments',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-departments/services',
        'language' => 'und',
      ),
    ),
  );
  return $terms;
}
